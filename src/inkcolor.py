#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
inkcolor.py - RGB(A) color representations in SVG paints

Copyright (C) 2016 ~suv, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


Supported RGB representations:
     * RGB
     * RGBA
    Python built-in:
     * HLS (Hue Lightness Saturation)
     * HSV (Hue Saturation Value)
    CSS3:
     * HSL (Hue Saturation Lightness)
     * HSLA

TODO: Investigate other color spaces:
     * HSB
     * HSI
     * YIQ (NTSC)  (useful e.g. for sepia effect)
     * YUV (PAL)


InkColor class:

    Input/output formats:
        RGB as list of integers
        RGB as list of floats
        RGB as hex string
        RGB as CSS3 color (partial)
        RGB as long int

        RGBA as list of integers
        RGBA as list of floats
        RGBA as hex string
        RGBA as CSS3 color (partial)
        RGBA as long int

        HLS as list of floats
        HSV as list of floats
        HSL as list of floats
        HLSA as list of floats
        HSVA as list of floats
        HSLA as list of floats

        style dict/string for fill
        style dict/string for stroke
        style dict/string for stop-color (gradient)
        style dict/string for flood-color (filter)
        style dict/string for lighting-color (filter)
        style dict/string for solid-color (named colors in SVG2)
        style dict/string for color

"""
# pylint: disable=too-many-public-methods
# pylint: disable=too-many-lines

# standard library
import colorsys
import random

# local library
from simplestyle import svgcolors


# integer types

def int_to_signed32(num):
    """Converted int to signed 32bit int.

    Source:
        http://stackoverflow.com/a/37095855
    """
    num = num & 0xffffffff
    return (num ^ 0x80000000) - 0x80000000


# util

def int_to_float(val):
    """Convert int *val* to float."""
    return val / 255.0


def float_to_int(val):
    """Convert float *val* to int."""
    # TODO: truncate or round?
    return int(round(val * 255))


def string_to_dict(s):
    """Convert string *s* to dict (from simplestyle)."""
    # pylint: disable=invalid-name
    if s is None:
        return {}
    else:
        return dict([[x.strip() for x in i.split(":")]
                     for i in s.split(";") if len(i.strip())])


def dict_to_string(d):
    """Convert dict *d* to string (from simplestyle)."""
    # pylint: disable=invalid-name
    return ";".join([key + ":" + str(val) for key, val in d.items()])


def clamp(minimum, val, maximum):
    """Clamp value to min/max values."""
    return max(minimum, min(val, maximum))


def clamp_float(val):
    """Clamp float *val* to 0.0 - 1.0."""
    return clamp(0.0, val, 1.0)


def clamp_int(val):
    """Clamp int *val* to 0 - 255."""
    return clamp(0, val, 255)


def normalize_int(val):
    """Return *val* (int or float) as integer clamped to 0-255."""
    if isinstance(val, int):
        return clamp_int(val)
    elif isinstance(val, float):
        return clamp_int(float_to_int(val))
    else:
        return 0  # TODO: normalize_int - fallback value or assert?


def normalize_float(val):
    """Return *val* (int or float) as float clamped to 0.0-1.0."""
    if isinstance(val, int):
        return clamp_float(int_to_float(val))
    elif isinstance(val, float):
        return clamp_float(val)
    else:
        return 0.0  # TODO: normalize_float - fallback value or assert?


def normalize_hex(string):
    """Normalize hex *string* representing RGBA color."""
    if len(string) and string[0] == '#':
        string = string[1:]
    if len(string) == 3:  # CSS shorthand
        string = ''.join(2 * c for c in string)
    if len(string) == 6:  # no alpha, append 'ff' for opaque
        string += "ff"
    if len(string) == 8:  # check final length
        return string.lower()
    else:
        return "000000ff"  # TODO: normalize_hex - fallback value or assert?


def normalize_angle(val, degree=False):
    """Normalize *val* (float or int) as angle (degree) between 0-360."""
    if isinstance(val, (int, float)):
        if not degree:
            return ((val % 1) + 1) % 1
        else:
            return ((val % 360) + 360) % 360
    else:
        return 0  # TODO: normalize_angle - fallback value or assert?


def funct_to_list(string):
    """Parse string with functional notation, return list of numeric values."""
    assert isinstance(string, str)
    mode, value = string.lower().split('(')  # split at opening parenthesis
    value = value.split(')')[0]              # remove closing parenthesis
    alist = []
    for i, val in enumerate(c.strip() for c in value.split(',')):
        if mode.startswith('rgb'):
            if i < 3:           # red, green, blue as percentages or integers
                if val.endswith('%'):
                    alist.append(normalize_float(float(val[:-1]) / 100.0))
                else:
                    alist.append(normalize_int(int(val)))
        elif mode.startswith('hsl'):
            if i == 0:          # hue in degrees
                alist.append(normalize_angle(float(val) / 360.0))
            elif i < 3:         # saturation, lightness as percentages
                if val.endswith('%'):
                    alist.append(normalize_float(float(val[:-1]) / 100.0))
        if i == 3:              # alpha in range 0.0-1.0
            alist.append(normalize_float(float(val)))
    if mode.endswith('a'):  # rgba, hsla
        assert len(alist) == 4
        return alist
    else:  # rgb, hsl
        assert len(alist) == 3
        return alist


def rgba_int_to_hex(red, green, blue, alpha=255):
    """Convert *red*, *green*, *blue* ints and *alpha* float to hex string."""
    _alpha = normalize_int(alpha)
    for val in (red, green, blue, _alpha):
        assert isinstance(val, int)
    return normalize_hex("#" +
                         format(red, '02x') +
                         format(green, '02x') +
                         format(blue, '02x') +
                         format(_alpha, '02x'))


def hex_to_rgba_int(string):
    """Convert normalized hex *string* to list of integers (RGBA)."""
    assert isinstance(string, str)
    hex_value = int(normalize_hex(string), 16)
    return ((hex_value >> 24) & 0xff,
            (hex_value >> 16) & 0xff,
            (hex_value >> 8) & 0xff,
            hex_value & 0xff)


def rgba_float_to_hex(red, green, blue, alpha=1.0):
    """Convert *red*, *green*, *blue*, *alpha* floats to hex string."""
    for val in (red, green, blue, alpha):
        assert isinstance(val, float)
    return normalize_hex("#" +
                         format(normalize_int(red), '02x') +
                         format(normalize_int(green), '02x') +
                         format(normalize_int(blue), '02x') +
                         format(normalize_int(alpha), '02x'))


def hex_to_rgba_float(string):
    """Convert normalized hex *string* to list of floats (RGBA)."""
    assert isinstance(string, str)
    hex_value = int(normalize_hex(string), 16)
    return (normalize_float((hex_value >> 24) & 0xff),
            normalize_float((hex_value >> 16) & 0xff),
            normalize_float((hex_value >> 8) & 0xff),
            normalize_float(hex_value & 0xff))


def rgba_int_to_long(red, green, blue, alpha=255):
    """Convert *red*, *green*, *blue*, *alpha* ints to Long."""
    _alpha = normalize_int(alpha)
    for val in (red, green, blue, _alpha):
        assert isinstance(val, int)
    return ((red << 24) +
            (green << 16) +
            (blue << 8) +
            _alpha)


def long_to_rgba_int(val):
    """Convert long int *val* to list of integers (RGBA)."""
    assert isinstance(val, int)
    return ((val >> 24) & 255,
            (val >> 16) & 255,
            (val >> 8) & 255,
            val & 255)


def rgba_float_to_long(red, green, blue, alpha=1.0):
    """Convert *red*, *green*, *blue*, *alpha* floats to Long."""
    for val in (red, green, blue, alpha):
        assert isinstance(val, float)
    return ((normalize_int(red) << 24) +
            (normalize_int(green) << 16) +
            (normalize_int(blue) << 8) +
            normalize_int(alpha))


def long_to_rgba_float(val):
    """Convert long int *val* to list of floats (RGBA)."""
    assert isinstance(val, int)
    return (normalize_float((val >> 24) & 255),
            normalize_float((val >> 16) & 255),
            normalize_float((val >> 8) & 255),
            normalize_float(val & 255))


def rgb_to_hsl(r, g, b):
    """Convert *r*, *g*, *b* floats to HSL (from coloreffect)"""
    # pylint: disable=invalid-name
    rgb_max = max(max(r, g), b)
    rgb_min = min(min(r, g), b)
    delta = rgb_max - rgb_min
    hsl = [0.0, 0.0, 0.0]
    hsl[2] = (rgb_max + rgb_min)/2.0
    if delta == 0:
        hsl[0] = 0.0
        hsl[1] = 0.0
    else:
        if hsl[2] <= 0.5:
            hsl[1] = delta / (rgb_max + rgb_min)
        else:
            hsl[1] = delta / (2 - rgb_max - rgb_min)
        if r == rgb_max:
            hsl[0] = (g - b) / delta
        else:
            if g == rgb_max:
                hsl[0] = 2.0 + (b - r) / delta
            else:
                if b == rgb_max:
                    hsl[0] = 4.0 + (r - g) / delta
        hsl[0] = hsl[0] / 6.0
        if hsl[0] < 0:
            hsl[0] = hsl[0] + 1
        if hsl[0] > 1:
            hsl[0] = hsl[0] - 1
    return hsl


def _hue_2_rgb(v1, v2, h):
    """Helper function for HLS -> RGB conversion (from coloreffect)."""
    # pylint: disable=invalid-name
    if h < 0:
        h += 6.0
    if h > 6:
        h -= 6.0
    if h < 1:
        return v1 + (v2 - v1) * h
    if h < 3:
        return v2
    if h < 4:
        return v1 + (v2 - v1) * (4 - h)
    return v1


def hsl_to_rgb(h, s, l):
    """Convert *h*, *s*, *l* floats to RGB (from coloreffect)."""
    # pylint: disable=invalid-name
    rgb = [0, 0, 0]
    if s == 0:
        rgb[0] = l
        rgb[1] = l
        rgb[2] = l
    else:
        if l < 0.5:
            v2 = l * (1 + s)
        else:
            v2 = l + s - l*s
        v1 = 2*l - v2
        rgb[0] = _hue_2_rgb(v1, v2, h*6 + 2.0)
        rgb[1] = _hue_2_rgb(v1, v2, h*6)
        rgb[2] = _hue_2_rgb(v1, v2, h*6 - 2.0)
    return rgb


def lookup_kw(key):
    """Return value for color keyword *key* in CSS color names or None."""
    if key in svgcolors:
        return svgcolors[key]
    else:
        return None


def lookup_hex(string):
    """Return keyword for hex *string* in CSS3 color names or *string*."""
    string = '#' + normalize_hex(string)[:6]
    try:
        return (key for key, val in svgcolors.items() if val == string).next()
    except StopIteration:
        pass
    return string


def is_color(string):
    """Determine if its a color we can use. If not, leave it unchanged."""
    if string.startswith('#') and (len(string) == 4 or len(string) == 7):
        return True
    if string.lower() in svgcolors:
        return True
    if string.startswith('rgb') or string.startswith('hsl'):
        return True
    return False


# ----- base class -----

class InkColor(object):
    """Base class to store an RGB color with alpha."""
    # pylint: disable=too-many-instance-attributes

    def __init__(self):
        """Store color as RGBA floats."""
        # Fallback: fully opaque black
        self._rgb_r = 0.0         # float 0.0-1.0
        self._rgb_g = 0.0         # float 0.0-1.0
        self._rgb_b = 0.0         # float 0.0-1.0
        self._rgb_a = 1.0         # float 0.0-1.0

    # Special methods

    def __repr__(self):
        return 'rgba({0}, {1}, {2}, {3})'.format(
            self.red,
            self.green,
            self.blue,
            self.alpha)

    def __str__(self):
        return self.rgb_hex()

    def __hex__(self):
        return '0x{}'.format(self.rgb_hex()[1:])

    def __long__(self):
        return self.rgba_long()

    def __eq__(self, other):
        assert isinstance(other, InkColor)
        # compare int values for r, g, b - or use 'is_near()'?
        return (other.red == self.red and
                other.green == self.green and
                other.blue == self.blue and
                other.alpha == self.alpha)

    def __ne__(self, other):
        assert isinstance(other, InkColor)
        # compare int values for r, g, b - or use 'is_near()'?
        return (other.red != self.red or
                other.green != self.green or
                other.blue != self.blue or
                other.alpha != self.alpha)

    def __neg__(self):
        # from share/extensions/color_negative.py
        new_color = type(self)()
        new_color.rgb_r = 1.0 - self.rgb_r
        new_color.rgb_g = 1.0 - self.rgb_g
        new_color.rgb_b = 1.0 - self.rgb_b
        new_color.rgb_a = self.rgb_a
        return new_color

    def __invert__(self):
        # TODO: verify: is invert the same as negate for RGB-based color?
        return self.__neg__()

    def __copy__(self):
        new_color = type(self)()
        new_color.__dict__.update(self.__dict__)
        return new_color

    def __add__(self, other):
        # assert isinstance(other, InkColor)
        # TODO: to be considered: blend (add) InkColor instances?
        pass

    def __sub__(self, other):
        # assert isinstance(other, InkColor)
        # TODO: to be considered: blend (subtract) InkColor instances?
        pass

    # Color component properties (returned as ints) with setters

    @property
    def red(self):
        """Get or set red."""
        return normalize_int(self._rgb_r)

    @red.setter
    def red(self, val):
        self._rgb_r = normalize_float(val)

    @property
    def green(self):
        """Get or set green."""
        return normalize_int(self._rgb_g)

    @green.setter
    def green(self, val):
        self._rgb_g = normalize_float(val)

    @property
    def blue(self):
        """Get or set blue."""
        return normalize_int(self._rgb_b)

    @blue.setter
    def blue(self, val):
        self._rgb_b = normalize_float(val)

    @property
    def alpha(self):
        """Get or set alpha."""
        return self._rgb_a

    @alpha.setter
    def alpha(self, val):
        self._rgb_a = normalize_float(val)

    # RGBA properties (returned as floats) with setters

    @property
    def rgb_r(self):
        """Get or set rgb_r (red)."""
        return self._rgb_r

    @rgb_r.setter
    def rgb_r(self, val):
        self._rgb_r = normalize_float(val)

    @property
    def rgb_g(self):
        """Get or set rgb_g (green)."""
        return self._rgb_g

    @rgb_g.setter
    def rgb_g(self, val):
        self._rgb_g = normalize_float(val)

    @property
    def rgb_b(self):
        """Get or set rgb_b (blue)."""
        return self._rgb_b

    @rgb_b.setter
    def rgb_b(self, val):
        self._rgb_b = normalize_float(val)

    @property
    def rgb_a(self):
        """Get or set rgb_a (alpha)."""
        return self._rgb_a

    @rgb_a.setter
    def rgb_a(self, val):
        self._rgb_a = normalize_float(val)

    # HLS, HSV, HSL properties with setters

    @property
    def hls_h_deg(self):
        """Get hue of HLS representation in degrees."""
        return self.rgb_to_hls()[0] * 360

    @property
    def hls_h(self):
        """Get or set hue of HLS representation."""
        return self.rgb_to_hls()[0]

    @hls_h.setter
    def hls_h(self, val, degree=False):
        _, lightness, saturation = self.rgb_to_hls()
        _val = normalize_angle(val / 360.0 if degree else val)
        self.from_hls(_val, lightness, saturation)

    @property
    def hls_l(self):
        """Get or set lightness of HLS representation."""
        return self.rgb_to_hls()[1]

    @hls_l.setter
    def hls_l(self, val):
        hue, _, saturation = self.rgb_to_hls()
        self.from_hls(hue, normalize_float(val), saturation)

    @property
    def hls_s(self):
        """Get or set saturation of HLS representation."""
        return self.rgb_to_hls()[2]

    @hls_s.setter
    def hls_s(self, val):
        hue, lightness, _ = self.rgb_to_hls()
        self.from_hls(hue, lightness, normalize_float(val))

    @property
    def hsv_h_deg(self):
        """Get hue of HSV representation in degrees."""
        return self.rgb_to_hsv()[0] * 360

    @property
    def hsv_h(self):
        """Get or set hue of HSV representation."""
        return self.rgb_to_hsv()[0]

    @hsv_h.setter
    def hsv_h(self, val, degree=False):
        _, saturation, value = self.rgb_to_hsv()
        _val = normalize_angle(val / 360.0 if degree else val)
        self.from_hsv(_val, saturation, value)

    @property
    def hsv_s(self):
        """Get or set saturation of HSV representation."""
        return self.rgb_to_hsv()[1]

    @hsv_s.setter
    def hsv_s(self, val):
        hue, _, value = self.rgb_to_hsv()
        self.from_hsv(hue, normalize_float(val), value)

    @property
    def hsv_v(self):
        """Get or set value of HSV representation."""
        return self.rgb_to_hsv()[2]

    @hsv_v.setter
    def hsv_v(self, val):
        hue, saturation, _ = self.rgb_to_hsv()
        self.from_hsv(hue, saturation, normalize_float(val))

    @property
    def hsl_h_deg(self):
        """Get hue of HLS representation in degrees."""
        return self.rgb_to_hsl()[0] * 360

    @property
    def hsl_h(self):
        """Get or set hue of HLS representation."""
        return self.rgb_to_hsl()[0]

    @hsl_h.setter
    def hsl_h(self, val, degree=False):
        _, saturation, lightness = self.rgb_to_hsl()
        _val = normalize_angle(val / 360.0 if degree else val)
        self.from_hsl(_val, saturation, lightness)

    @property
    def hsl_s(self):
        """Get or set lightness of HSL representation."""
        return self.rgb_to_hsl()[1]

    @hsl_s.setter
    def hsl_s(self, val):
        hue, _, lightness = self.rgb_to_hsl()
        self.from_hsl(hue, normalize_float(val), lightness)

    @property
    def hsl_l(self):
        """Get or set lightness of HSL representation."""
        return self.rgb_to_hsl()[2]

    @hsl_l.setter
    def hsl_l(self, val):
        hue, saturation, _ = self.rgb_to_hsl()
        self.from_hsl(hue, saturation, normalize_float(val))

    @property
    def hsl_a(self):
        """Get or set alpha of HSLA representation."""
        return self.rgb_a

    @hsl_a.setter
    def hsl_a(self, val):
        self.rgb_a = normalize_float(val)

    # Input RGBA (ints, floats, hex, long)
    # TODO: investigate using @classmethod for input (create new instance)

    def from_num(self, red=0, green=0, blue=0, alpha=None):
        """Set RGBA values from int or float values."""
        for val in (red, green, blue):
            assert isinstance(val, (int, float))
        self.rgb_r = red
        self.rgb_g = green
        self.rgb_b = blue
        if alpha is not None:
            assert isinstance(alpha, (int, float))
            self.rgb_a = alpha

    def from_int(self, red=0, green=0, blue=0, alpha=None):
        """Set RGBA values from integer values."""
        for val in (red, green, blue):
            assert isinstance(val, int)
        self.from_num(red, green, blue, alpha)

    def from_float(self, red=0.0, green=0.0, blue=0.0, alpha=None):
        """Set RGBA values from float values."""
        for val in (red, green, blue):
            assert isinstance(val, float)
        self.from_num(red, green, blue, alpha)

    def from_hex(self, string):
        """Set RGBA values from hex *string*."""
        alpha = self.alpha
        self.from_num(*hex_to_rgba_int(string))
        self.alpha = alpha if len(string) < 8 else self.alpha

    def from_css(self, string):
        """Set RGBA values from CSS color notation *string*."""
        assert isinstance(string, str)
        if string.startswith('#'):
            self.from_hex(string)
        elif string.startswith('rgb('):
            self.from_num(*funct_to_list(string), alpha=self.alpha)
        elif string.startswith('rgba('):
            self.from_num(*funct_to_list(string))
        elif string.startswith('hsl('):
            self.from_hsla(*funct_to_list(string), alpha=self.alpha)
        elif string.startswith('hsla('):
            self.from_hsla(*funct_to_list(string))
        elif not string.startswith('url('):
            hex_val = lookup_kw(string)
            if hex_val is not None:
                self.from_hex(hex_val)
        else:
            pass

    def from_long(self, long_int):
        """Set RGBA values from long integer."""
        self.from_num(*long_to_rgba_int(long_int))

    def from_signed(self, signed_long):
        """Set RGBA values from signed 32bit integer."""
        self.from_long(signed_long)

    # Input RGB(A) from other representations (HLS, HSV, HSL, HSLA)
    # TODO: investigate using @classmethod for input (create new instance)

    def from_hls(self, hue=0.0, lightness=0.0, saturation=1.0, degree=False):
        """Set RGB values from HLS values."""
        _hue = normalize_angle(hue / 360.0 if degree else hue)
        _lightness = normalize_float(lightness)
        _saturation = normalize_float(saturation)
        self.from_float(*colorsys.hls_to_rgb(_hue, _lightness, _saturation))

    def from_hsv(self, hue=0.0, saturation=0.0, value=1.0, degree=False):
        """Set RGB values from HSV values."""
        _hue = normalize_angle(hue / 360.0 if degree else hue)
        _saturation = normalize_float(saturation)
        _value = normalize_float(value)
        self.from_float(*colorsys.hsv_to_rgb(_hue, _saturation, _value))

    def from_hsl(self, hue=0.0, saturation=1.0, lightness=0.0, degree=False):
        """Set RGB values from HSL values."""
        # self.from_hls(hue, lightness, saturation)
        _hue = normalize_angle(hue / 360.0 if degree else hue)
        _saturation = normalize_float(saturation)
        _lightness = normalize_float(lightness)
        self.from_float(*hsl_to_rgb(_hue, _saturation, _lightness))

    def from_hsla(self, hue=0.0, saturation=1.0, lightness=0.0, alpha=1.0,
                  degree=False):
        """Set RGBA values from HSLA values."""
        # pylint: disable=too-many-arguments
        self.from_hsl(hue, saturation, lightness, degree)
        self.alpha = alpha

    # Input RGBA from SVG paints

    def from_style_dict(self, style, prop='fill'):
        """Set RGBA values from *style* dict for property *prop*."""
        # pylint: disable=too-many-branches
        assert isinstance(style, dict)
        if prop == 'fill' and 'fill' in style:
            self.from_css(style['fill'])
            if 'fill-opacity' in style:
                self.alpha = normalize_float(float(style['fill-opacity']))
        elif prop == 'stroke':
            self.from_css(style['stroke'])
            if 'stroke-opacity' in style:
                self.alpha = normalize_float(float(style['stroke-opacity']))
        elif ((prop == 'stop' or prop == 'stop-color') and
              'stop-color' in style):
            self.from_css(style['stop-color'])
            if 'stop-opacity' in style:
                self.alpha = normalize_float(float(style['stop-opacity']))
        elif ((prop == 'flood' or prop == 'flood-color') and
              'flood-color' in style):
            self.from_css(style['flood-color'])
            if 'flood-opacity' in style:
                self.alpha = normalize_float(float(style['flood-opacity']))
        elif ((prop == 'lighting' or prop == 'lighting-color') and
              'lighting-color' in style):
            self.from_css(style['lighting-color'])
        elif ((prop == 'solid' or prop == 'solid-color') and
              'solid-color' in style):  # SVG2
            self.from_css(style['solid-color'])
            if 'solid-color' in style:
                self.alpha = normalize_float(float(style['solid-opacity']))
        else:
            if 'color' in style:
                self.from_css(style['color'])
            if 'opacity' in style:
                self.alpha = normalize_float(float(style['opacity']))

    def from_style_str(self, style, prop='fill'):
        """Set RGBA values from *style* string for property *prop*."""
        assert isinstance(style, str)
        self.from_style_dict(string_to_dict(style), prop)

    def from_random(self, alpha=False):
        """Set RGBA values from random numbers."""
        self.rgb_r = random.random()
        self.rgb_g = random.random()
        self.rgb_b = random.random()
        if alpha:
            self.rgb_a = random.random()

    # Output RGB (ints, floats, hex, long)

    def rgb_float(self):
        """Return RGB formatted as list of floats."""
        return [normalize_float(i) for i in
                (self.rgb_r, self.rgb_g, self.rgb_b)]

    def rgb_int(self):
        """Return RGB formatted as list of ints."""
        return [normalize_int(i) for i in
                (self.rgb_r, self.rgb_g, self.rgb_b)]

    def rgb_hex(self):
        """Return RGB formatted as string of hex digits."""
        return '#{0:02x}{1:02x}{2:02x}'.format(*self.rgb_int())

    def rgb_css(self, mode="rgb"):
        """Return RGB as functional notation (rgb, hsl) or color keyword."""
        # TODO: limit precision of floats in formatted output?
        if mode == 'rgb':
            return 'rgb({0},{1},{2})'.format(*self.rgb_int())
        elif mode == 'hsl':
            return 'hsl({0},{1}%,{2}%)'.format(
                self.hsl_h_deg,
                self.hsl_s * 100,
                self.hsl_l * 100)
        elif mode == 'kw':
            return lookup_hex(self.rgb_hex())
        else:  # return hex string
            return self.rgb_hex()

    def rgb_long(self):
        """Return RGB as long int."""
        return ((self.red << 24) +
                (self.green << 16) +
                (self.blue << 8))

    def rgb_signed(self):
        """Return RGB as signed 32bit int."""
        return int_to_signed32(self.rgb_long())

    # Output RGB to other representations (HLS, HSV, HSL)

    def rgb_to_hls(self):
        """Return RGB values converted to HLS list."""
        return colorsys.rgb_to_hls(*self.rgb_float())

    def rgb_to_hsv(self):
        """Return RGB values converted to HSV list."""
        return colorsys.rgb_to_hsv(*self.rgb_float())

    def rgb_to_hsl(self):
        """Return RGB values converted to HSL list."""
        # hue, lightness, saturation = self.rgb_to_hls()
        hue, saturation, lightness = rgb_to_hsl(*self.rgb_float())
        return tuple([hue, saturation, lightness])

    # Output RGB for SVG paints

    def rgb_to_style_dict(self, prop='fill'):
        """Return RGB as dict with style properties for *prop* color."""
        sdict = {}
        if prop == 'fill':
            sdict = {
                'fill': self.rgb_hex(),
            }
        elif prop == 'stroke':
            sdict = {
                'stroke': self.rgb_hex(),
            }
        elif prop == 'stop' or prop == 'stop-color':
            sdict = {
                'stop-color': self.rgb_hex(),
            }
        elif prop == 'flood' or prop == 'flood-color':
            sdict = {
                'flood-color': self.rgb_hex(),
            }
        elif prop == 'lighting' or prop == 'lighting-color':
            sdict = {
                'lighting-color': self.rgb_hex(),
            }
        elif prop == 'solid' or prop == 'solid-color':  # SVG2
            sdict = {
                'solid-color': self.rgb_hex(),
            }
        else:
            sdict = {
                'color': self.rgb_hex(),
            }
        return sdict

    def rgb_to_style_str(self, prop='fill'):
        """Return RGB as inline CSS style string for *prop* color."""
        return dict_to_string(self.rgb_to_style_dict(prop))

    # Output RGBA (ints, floats, hex, long)

    def rgba_float(self):
        """Return RGBA formatted as list of floats."""
        return self.rgb_float() + [normalize_float(self.alpha)]

    def rgba_int(self):
        """Return RGBA formatted as list of ints."""
        return self.rgb_int() + [normalize_int(self.alpha)]

    def rgba_hex(self):
        """Return RGBA formatted as string of hex digits."""
        return self.rgb_hex() + format(normalize_int(self.alpha), '02x')

    def rgba_css(self, mode="rgba"):
        """Return RGBA as functional notation (rgba, hsla)."""
        # TODO: limit precision of floats in formatted output?
        #       e.g. 'hsl({0:.2},{1:.2}%,{2:.2}%,{3:.5})'
        if mode == 'rgba':
            # return repr(self)
            return 'rgba({0},{1},{2},{3})'.format(
                self.red,
                self.green,
                self.blue,
                self.alpha)
        elif mode == 'hsla':
            return 'hsl({0},{1}%,{2}%,{3})'.format(
                self.hsl_h_deg,
                self.hsl_s * 100,
                self.hsl_l * 100,
                self.alpha)
        else:  # return hex string
            return self.rgba_hex()  # NOTE: not a valid CSS color format!

    def rgba_long(self):
        """Return RGBA as long int."""
        return self.rgb_long() + normalize_int(self.alpha)

    def rgba_signed(self):
        """Return RGBA as signed 32bit int."""
        return int_to_signed32(self.rgba_long())

    # Output RGBA to other representations (HSLA, HSVA, HLSA)

    def rgba_to_hlsa(self):
        """Return RGBA values converted to HLSA list."""
        return tuple(list(self.rgb_to_hls()) + [self.alpha])

    def rgba_to_hsva(self):
        """Return RGBA values converted to HSVA list."""
        return tuple(list(self.rgb_to_hsv()) + [self.alpha])

    def rgba_to_hsla(self):
        """Return RGBA values converted to HSLA list."""
        return tuple(list(self.rgb_to_hsl()) + [self.alpha])

    # Output RGBA for SVG paints

    def rgba_to_style_dict(self, prop='fill'):
        """Return RGBA as dict with style properties for *prop* color."""
        sdict = {}
        if prop == 'fill':
            sdict = {
                'fill': self.rgb_hex(),
                'fill-opacity': self.alpha,
            }
        elif prop == 'stroke':
            sdict = {
                'stroke': self.rgb_hex(),
                'stroke-opacity': self.alpha,
            }
        elif prop == 'stop' or prop == 'stop-color':
            sdict = {
                'stop-color': self.rgb_hex(),
                'stop-opacity': self.alpha,
            }
        elif prop == 'flood' or prop == 'flood-color':
            sdict = {
                'flood-color': self.rgb_hex(),
                'flood-opacity': self.alpha,
            }
        elif prop == 'lighting' or prop == 'lighting-color':
            sdict = {
                'lighting-color': self.rgb_hex(),
            }
        elif prop == 'solid' or prop == 'solid-color':  # SVG2
            sdict = {
                'solid-color': self.rgb_hex(),
                'solid-opacity': self.alpha,
            }
        else:
            sdict = {
                'color': self.rgb_hex(),
                'opacity': self.alpha,
            }
        return sdict

    def rgba_to_style_str(self, prop='fill'):
        """Return RGBA as inline CSS style string for *prop* color."""
        return dict_to_string(self.rgba_to_style_dict(prop))


# ----- sub-classes -----

class InkColorRGBAInt(InkColor):
    """Sub-class to define color with int values."""

    def __init__(self, red=0, green=0, blue=0, alpha=255):
        """Store Inkscape color as RGBA floats."""
        super(InkColorRGBAInt, self).__init__()

        self.from_int(red, green, blue, alpha)


class InkColorRGBAFloat(InkColor):
    """Sub-class to define color with float values."""

    def __init__(self, red=0, green=0, blue=0, alpha=1.0):
        """Store Inkscape color as RGBA floats."""
        super(InkColorRGBAFloat, self).__init__()

        self.from_float(red, green, blue, alpha)


class InkColorRGBAHex(InkColor):
    """Sub-class to define color with hex string."""

    def __init__(self, string="000000ff"):
        """Store Inkscape color as RGBA floats."""
        super(InkColorRGBAHex, self).__init__()

        self.from_hex(string)


class InkColorRGBACSS(InkColor):
    """Sub-class to define color with hex string."""

    def __init__(self, string="#000000ff"):
        """Store Inkscape color as RGBA floats."""
        super(InkColorRGBACSS, self).__init__()

        self.from_css(string)


class InkColorRGBALong(InkColor):
    """Sub-class to define color with long int or signed 32bit int."""

    def __init__(self, long_int=0):
        """Store Inkscape color (long int) as RGBA floats."""
        super(InkColorRGBALong, self).__init__()

        self.from_long(long_int)


class InkColorHLS(InkColor):
    """Sub-class to define color with HLS values."""

    def __init__(self, hue=0.0, lightness=0.0, saturation=1.0, degree=False):
        """Store Inkscape color as RGBA floats."""
        super(InkColorHLS, self).__init__()

        self.from_hls(hue, lightness, saturation, degree)


class InkColorHSV(InkColor):
    """Sub-class to define color with HSV values."""

    def __init__(self, hue=0.0, saturation=0.0, value=1.0, degree=False):
        """Store Inkscape color as RGBA floats."""
        super(InkColorHSV, self).__init__()

        self.from_hsv(hue, saturation, value, degree)


class InkColorHSL(InkColor):
    """Sub-class to define color with HSL values."""

    def __init__(self, hue=0.0, saturation=1.0, lightness=0.0, degree=False):
        """Store Inkscape color as RGBA floats."""
        super(InkColorHSL, self).__init__()

        self.from_hsl(hue, saturation, lightness, degree)


class InkColorHSLA(InkColor):
    """Sub-class to define color with HSLA values."""

    def __init__(self, hue=0.0, saturation=1.0, lightness=0.0, alpha=1.0,
                 degree=False):
        """Store Inkscape color as RGBA floats."""
        # pylint: disable=too-many-arguments
        super(InkColorHSLA, self).__init__()

        self.from_hsla(hue, saturation, lightness, alpha, degree)


# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79

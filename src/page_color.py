#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
page_color.py - Change page color

Copyright (C) 2016 ~suv <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# standard library
# local library
import inkex
from colorutils import *


# convenience function for SVG style properties

def get_color_from_int(i):
    """Convert signed long RGBA to hex color string and float alpha."""
    r = (i >> 24) & 255
    g = (i >> 16) & 255
    b = (i >> 8) & 255
    c = "#%02x%02x%02x" % (r, g, b)
    o = (i & 255) / 255.0
    return (c, o)


def set_pagecolor(node, color, opacity):
    """Set page color and opactity in node."""
    node.set('pagecolor', color)
    node.set(inkex.addNS('pageopacity', 'inkscape'), str(round(opacity, 3)))


def report_color(rgba_long):
    """Report color."""
    rgba_hex = long_to_hex_rgba(rgba_long)
    rgba_int = long_to_int_rgba(rgba_long)
    rgba_float = long_to_float_rgba(rgba_long)
    rgb_hex = long_to_hex_rgb(rgba_long)
    rgb_int = long_to_int_rgb(rgba_long)
    inkex.debug('RGBA Long:  {}'.format(rgba_long))
    inkex.debug('RGBA Hex:   #{}'.format(rgba_hex))
    inkex.debug('RGBA Int:   {}'.format(rgba_int))
    inkex.debug('RGBA Float: {}'.format(rgba_float))
    inkex.debug('RGB  Hex:   #{}'.format(rgb_hex))
    inkex.debug('RGB  Int:   {}'.format(rgb_int))
    inkex.debug('==========')
    inkex.debug('hex2long RGBA: {}'.format(hex_to_long_rgba(rgba_hex)))
    inkex.debug('int2long RGBA: {}'.format(int_to_long_rgba(rgba_int)))
    inkex.debug('hex2long RGB : {}'.format(hex_to_long_rgba(rgb_hex)))
    inkex.debug('int2long RGB : {}'.format(int_to_long_rgba(rgb_int)))
    inkex.debug('==========')
    inkex.debug(
        'long alpha to int: {}'.format(long_to_int_alpha(rgba_long)[0]))
    inkex.debug(
        'long alpha to hex: 0x{}'.format(long_to_hex_alpha(rgba_long)))
    inkex.debug(
        'long alpha to float: {}'.format(long_to_float_alpha(rgba_long)[0]))
    inkex.debug('==========')
    inkex.debug(
        '"896839168 for blue": #{}'.format(long_to_hex_rgba(896839168)))
    inkex.debug('==========')
    inkex.debug(
        'typical use case: #{}, {}'.format(long_to_hex_rgb(rgba_long),
                                           long_to_float_alpha(rgba_long)[0]))
    inkex.debug('==========')


class PageColor(inkex.Effect):
    """Effect-based class to set page color of current document."""

    def __init__(self):
        inkex.Effect.__init__(self)

        # Color as signed long RGBA
        self.OptionParser.add_option("--nv_page_color",
                                     action="store",
                                     type="int",
                                     dest="nv_page_color",
                                     default=-256,
                                     help="Custom background color")
        # Color as string, opacity as float
        self.OptionParser.add_option("--nv_page_color_string",
                                     action="store",
                                     type="string",
                                     dest="nv_page_color_string",
                                     default="#0000ff",
                                     help="Page color")
        self.OptionParser.add_option("--nv_page_opacity_float",
                                     action="store",
                                     type="float",
                                     dest="nv_page_opacity_float",
                                     default="0.4",
                                     help="Page opacity")
        # misc
        self.OptionParser.add_option("--verbose",
                                     action="store",
                                     type="inkbool",
                                     dest="report",
                                     default=False,
                                     help="Report color value")
        # tabs
        self.OptionParser.add_option("--notebook_main",
                                     action="store",
                                     type="string",
                                     dest="notebook_main")

    def set_page_color_from_string(self, node):
        """Set page color from string input."""
        c = self.options.nv_page_color_string
        o = self.options.nv_page_opacity_float
        set_pagecolor(node, c, o)

    def set_page_color_from_rgba(self, node):
        """Set page color from color widget."""
        c, o = get_color_from_int(self.options.nv_page_color)
        set_pagecolor(node, c, o)

    def doit(self, node):
        """Dispatch setting attributes based on current tab."""
        if self.options.notebook_main == '"tab_page_color"':
            if self.options.report:
                report_color(self.options.nv_page_color)
            self.set_page_color_from_rgba(node)
        elif self.options.notebook_main == '"tab_page_color_string"':
            self.set_page_color_from_string(node)
        else:
            inkex.debug("active tab: %s" % self.options.notebook_main)

    def effect(self):
        """Main effect starts here."""
        named_view = self.getNamedView()
        self.doit(named_view)


if __name__ == '__main__':
    ME = PageColor()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79

#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
page_color2.py - Change page color

Copyright (C) 2016 ~suv <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# standard library
# local library
import inkex
import inkcolor as ic


# convenience function for SVG style properties

def set_pagecolor(node, col):
    """Set page color and opactity in node."""
    node.set('pagecolor', col.rgb_hex())
    node.set(inkex.addNS('pageopacity', 'inkscape'), str(round(col.alpha, 3)))


def report_color(rgba_long):
    """Report color."""
    c = ic.InkColorRGBALong(rgba_long)
    rgba_hex = c.rgba_hex()
    rgba_int = c.rgba_int()
    rgba_float = c.rgba_float()
    rgb_hex = c.rgb_hex()
    rgb_int = c.rgb_int()
    inkex.debug('RGBA Long:  {}'.format(rgba_long))
    inkex.debug('RGBA Hex:   {}'.format(rgba_hex))
    inkex.debug('RGBA Int:   {}'.format(rgba_int))
    inkex.debug('RGBA Float: {}'.format(rgba_float))
    inkex.debug('RGB  Hex:   {}'.format(rgb_hex))
    inkex.debug('RGB  Int:   {}'.format(rgb_int))
    inkex.debug('==========')

    d = ic.InkColor()
    d.from_hex(rgba_hex)
    inkex.debug('from_hex RGBA: {}'.format(repr(d)))
    inkex.debug('hex2long RGBA: {}'.format(d.rgba_long()))
    inkex.debug('hex2signed RGBA: {}'.format(d.rgba_signed()))
    d.from_int(*rgba_int)
    inkex.debug('from_int RGBA: {}'.format(repr(d)))
    inkex.debug('int2long RGBA: {}'.format(d.rgba_long()))
    inkex.debug('int2signed RGBA: {}'.format(d.rgba_signed()))
    d.from_hex(rgb_hex)
    inkex.debug('from_hex RGB : {}'.format(repr(d)))
    inkex.debug('hex2long RGB : {}'.format(d.rgba_long()))
    inkex.debug('hex2signed RGB : {}'.format(d.rgba_signed()))
    d.from_int(*rgb_int)
    inkex.debug('from_int RGB : {}'.format(repr(d)))
    inkex.debug('int2long RGB : {}'.format(d.rgba_long()))
    inkex.debug('int2signed RGB : {}'.format(d.rgba_signed()))
    inkex.debug('==========')
    d.from_long(rgba_long)
    inkex.debug('long alpha to int: {}'.format(int(d.alpha * 255)))
    inkex.debug('long alpha to hex: 0x{}'.format(d.rgba_hex()[-2]))
    inkex.debug('long alpha to float: {}'.format(d.alpha))
    inkex.debug('==========')
    d.from_long(896839168)
    inkex.debug('from_long: {}'.format(repr(d)))
    inkex.debug('"896839168 for blue": {}'.format(d.rgba_hex()))
    inkex.debug('==========')
    d.from_long(rgba_long)
    inkex.debug('typical use case: #{}, {}'.format(d.rgb_hex(), d.alpha))
    inkex.debug('==========')

    e = ic.InkColor()
    e.alpha = 0.9
    inkex.debug('test passing object to method: {}'.format(repr(e)))
    modify_alpha(e)
    inkex.debug('after running method: {}'.format(repr(e)))
    inkex.debug('==========')


def modify_alpha(ink_color):
    ink_color.alpha -= 0.1


class PageColor(inkex.Effect):
    """Effect-based class to set page color of current document."""

    def __init__(self):
        inkex.Effect.__init__(self)

        # Color as signed long RGBA
        self.OptionParser.add_option("--nv_page_color",
                                     action="store",
                                     type="int",
                                     dest="nv_page_color",
                                     default=-256,
                                     help="Custom background color")
        # Color as string, opacity as float
        self.OptionParser.add_option("--nv_page_color_string",
                                     action="store",
                                     type="string",
                                     dest="nv_page_color_string",
                                     default="#0000ff",
                                     help="Page color")
        self.OptionParser.add_option("--nv_page_opacity_float",
                                     action="store",
                                     type="float",
                                     dest="nv_page_opacity_float",
                                     default="0.4",
                                     help="Page opacity")
        # misc
        self.OptionParser.add_option("--verbose",
                                     action="store",
                                     type="inkbool",
                                     dest="report",
                                     default=False,
                                     help="Report color value")
        # tabs
        self.OptionParser.add_option("--notebook_main",
                                     action="store",
                                     type="string",
                                     dest="notebook_main")

    def set_page_color_from_string(self, node):
        """Set page color from string input."""
        c = ic.InkColorRGBAHex(self.options.nv_page_color_string)
        c.alpha = self.options.nv_page_opacity_float
        set_pagecolor(node, c)

    def set_page_color_from_rgba(self, node):
        """Set page color from color widget."""
        c = ic.InkColorRGBALong(self.options.nv_page_color)
        set_pagecolor(node, c)

    def doit(self, node):
        """Dispatch setting attributes based on current tab."""
        if self.options.notebook_main == '"tab_page_color"':
            if self.options.report:
                report_color(self.options.nv_page_color)
            self.set_page_color_from_rgba(node)
        elif self.options.notebook_main == '"tab_page_color_string"':
            self.set_page_color_from_string(node)
        else:
            inkex.debug("active tab: %s" % self.options.notebook_main)

    def effect(self):
        """Main effect starts here."""
        named_view = self.getNamedView()
        self.doit(named_view)


if __name__ == '__main__':
    ME = PageColor()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79

#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
colorutils.py - utilities to convert between color formats

Copyright (C) 2016 ~suv <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# standard library
import codecs


# integer types

def to_signed32(n):
    """Converted int to signed 32bit int.

    Source:
        http://stackoverflow.com/a/37095855
    """
    n = n & 0xffffffff
    return (n ^ 0x80000000) - 0x80000000


# convert from/to int, float, hex

def int_to_float(v):
    """Convert list of int values to list of floats."""
    return [i/255.0 for i in v]


def float_to_int(v):
    """Convert list of float values to list of ints."""
    return [int(i * 255) for i in v]


def int_to_hex(v):
    """Convert list of int values to hex string."""
    return codecs.encode("".join(chr(i) for i in v), 'hex')


def hex_to_int(v):
    """Convert hex string to list of int values."""
    if v[0] == '#':
        v = v[1:]
    return [ord(c) for c in codecs.decode(v, 'hex')]


def float_to_hex(v):
    """Convert list of float values to hex string."""
    return int_to_hex(float_to_int(v))


def hex_to_float(v):
    """Convert hex string to list of float values."""
    return int_to_float(hex_to_int(v))


# RGBA <-> RGBA

def long_to_int_rgba(v):
    """Convert signed long RGBA value to list of r, g, b, a int values."""
    r = ((v >> 24) & 255)
    g = ((v >> 16) & 255)
    b = ((v >> 8) & 255)
    o = ((v) & 255)
    return [r, g, b, o]


def int_to_long_rgba(v):
    """Convert list of r, g, b, a int values to signed long RGBA value."""
    if len(v) == 3:
        v.append(255)
    else:
        assert len(v) == 4
    r, g, b, a = v
    n = (r << 24) + (g << 16) + (b << 8) + a
    return to_signed32(n)


def long_to_float_rgba(v):
    """Convert signed long RGBA value to list of r, g, b, a float values."""
    return int_to_float(long_to_int_rgba(v))


def float_to_long_rgba(v):
    """Convert list of r, g, b, a float values to signed long RGBA value."""
    return int_to_long_rgba(float_to_int(v))


def long_to_hex_rgba(v):
    """Convert signed long RGBA value to list of r, g, b, a hex values."""
    return int_to_hex(long_to_int_rgba(v))


def hex_to_long_rgba(v):
    """Convert list of r, g, b, a hex values to signed long RGBA value."""
    return int_to_long_rgba(hex_to_int(v))


# RGBA <-> RGB, alpha

def long_to_int_rgb(v):
    """Convert signed long RGBA value to list of r, g, b int values."""
    return long_to_int_rgba(v)[:3]


def long_to_int_alpha(v):
    """Convert alpha value from signed long RGBA value to int."""
    return [long_to_int_rgba(v)[-1]]


def long_to_float_rgb(v):
    """Convert signed long RGBA value to list of r, g, b float values."""
    return int_to_float(long_to_int_rgb(v))


def long_to_float_alpha(v):
    """Convert alpha value from signed long RGBA value to float."""
    return int_to_float(long_to_int_alpha(v))


def long_to_hex_rgb(v):
    """Convert signed long RGBA value to list of r, g, b hex values."""
    return int_to_hex(long_to_int_rgb(v))


def long_to_hex_alpha(v):
    """Convert alpha value from signed long RGBA value to hex."""
    return int_to_hex(long_to_int_alpha(v))


class ColorUtil(object):
    """conversion functions as class methods (what for?)."""
    # pylint: disable=invalid-name

    # convert from/to int, float, hex

    @staticmethod
    def int_to_float(v):
        """Convert list of int values to list of floats."""
        return [i/255.0 for i in v]

    @staticmethod
    def float_to_int(v):
        """Convert list of float values to list of ints."""
        return [int(i * 255) for i in v]

    @staticmethod
    def int_to_hex(v):
        """Convert list of int values to hex string."""
        return codecs.encode("".join(chr(i) for i in v), 'hex')

    @staticmethod
    def hex_to_int(v):
        """Convert hex string to list of int values."""
        if v[0] == '#':
            v = v[1:]
        return [ord(c) for c in codecs.decode(v, 'hex')]

    @classmethod
    def float_to_hex(cls, v):
        """Convert list of float values to hex string."""
        return cls.int_to_hex(cls.float_to_int(v))

    @classmethod
    def hex_to_float(cls, v):
        """Convert hex string to list of float values."""
        return cls.int_to_float(cls.hex_to_int(v))

    # RGBA <-> RGBA

    @staticmethod
    def long_to_int_rgba(v):
        """Convert signed long RGBA value to list of r, g, b, a int values."""
        r = ((v >> 24) & 255)
        g = ((v >> 16) & 255)
        b = ((v >> 8) & 255)
        o = ((v) & 255)
        return [r, g, b, o]

    @staticmethod
    def int_to_long_rgba(v):
        """Convert list of r, g, b, a int values to signed long RGBA value."""
        if len(v) == 3:
            v.append(255)
        else:
            assert len(v) == 4
        r, g, b, a = v
        n = (r << 24) + (g << 16) + (b << 8) + a
        return to_signed32(n)

    @classmethod
    def long_to_float_rgba(cls, v):
        """Convert long RGBA value to list of r, g, b, a float values."""
        return cls.int_to_float(cls.long_to_int_rgba(v))

    @classmethod
    def float_to_long_rgba(cls, v):
        """Convert list of r, g, b, a float values to long RGBA value."""
        return cls.int_to_long_rgba(cls.float_to_int(v))

    @classmethod
    def long_to_hex_rgba(cls, v):
        """Convert signed long RGBA value to list of r, g, b, a hex values."""
        return cls.int_to_hex(cls.long_to_int_rgba(v))

    @classmethod
    def hex_to_long_rgba(cls, v):
        """Convert list of r, g, b, a hex values to signed long RGBA value."""
        return cls.int_to_long_rgba(cls.hex_to_int(v))

    # RGBA <-> RGB, alpha

    @classmethod
    def long_to_int_rgb(cls, v):
        """Convert signed long RGBA value to list of r, g, b int values."""
        return cls.long_to_int_rgba(v)[:3]

    @classmethod
    def long_to_int_alpha(cls, v):
        """Convert alpha value from signed long RGBA value to int."""
        return [cls.long_to_int_rgba(v)[-1]]

    @classmethod
    def long_to_float_rgb(cls, v):
        """Convert signed long RGBA value to list of r, g, b float values."""
        return cls.int_to_float(cls.long_to_int_rgb(v))

    @classmethod
    def long_to_float_alpha(cls, v):
        """Convert alpha value from signed long RGBA value to float."""
        return cls.int_to_float(cls.long_to_int_alpha(v))

    @classmethod
    def long_to_hex_rgb(cls, v):
        """Convert signed long RGBA value to list of r, g, b hex values."""
        return cls.int_to_hex(cls.long_to_int_rgb(v))

    @classmethod
    def long_to_hex_alpha(cls, v):
        """Convert alpha value from signed long RGBA value to hex."""
        return cls.int_to_hex(cls.long_to_int_alpha(v))


# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
